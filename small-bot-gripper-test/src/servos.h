#ifndef SERVO_H
#define SERVO_H
//begin of .h file

#include <Arduino.h>
#include <Servo.h>

Servo Grapperleft;
Servo Grapperight;
Servo Gripperleft;
Servo Gripperright;

void collectingposition() {
  Grapperleft.write(1);
  Grapperight.write(141);
}

void slotted() {
  Grapperleft.write(21);
  Grapperight.write(101);
}

void idle() {
  Grapperleft.write(121);
  Grapperight.write(1);
}

void Gripper_open() {
  Gripperleft.write(180);
  Gripperright.write(0);
}

#endif