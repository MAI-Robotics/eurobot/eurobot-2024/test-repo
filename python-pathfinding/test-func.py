# pip install pathfinding
from pathfinding.core.diagonal_movement import DiagonalMovement
from pathfinding.core.grid import Grid
from pathfinding.finder.a_star import AStarFinder

# Create the matrix
matrix = [[1] * 300 for _ in range(200)]
grid = Grid(matrix=matrix)

currentPos = grid.node(5, 5)  # Start pos here
end = grid.node(0, 0)       # End pos here, overwritten by driveTo
#               x, y


finder = AStarFinder(diagonal_movement=DiagonalMovement.always)
path, runs = finder.find_path(currentPos, end, grid)
# create the finder

def updatePath():
    global path, runs
    grid.cleanup()
    finder = AStarFinder(diagonal_movement=DiagonalMovement.always)
    path, runs = finder.find_path(currentPos, end, grid)

def findNextXCoord(currentX=currentPos.x, currentY=currentPos.y, endX=end.x, endY=end.y):
    updatePath()
    if len(path) > 1:
      next_x = path[1][0]   # Nächste X-Koordinate
      return next_x
    else:
      return currentPos.x

def findNextYCoord():
    updatePath()
    if len(path) > 1:
      next_y = path[1][1]   # Nächste Y-Koordinate
      return next_y
    else:
      return currentPos.y

def driveTo(x, y):
   global currentPos
   global end
   end = grid.node(x, y)
   i=1
   while (currentPos.x != end.x and currentPos.y != end.y):  # solange wir noch nicht am ziel sind
      i = i + 1
      updatePath()

      #debugging purposes, replace with bot currentPos finding
      next_x = findNextXCoord()
      next_y = findNextYCoord()
      currentPos = grid.node(next_x, next_y)

      print("X: " + str(next_x) + " Y: " + str(next_y))

driveTo(50, 100)

#print('operations:', runs, 'path length:', len(path))
#print(grid.grid_str(path=path, start=currentPos, end=end))
