# pip install pathfinding

import time
from pathfinding.core.diagonal_movement import DiagonalMovement
from pathfinding.core.grid import Grid
from pathfinding.finder.a_star import AStarFinder

start_x = 12
start_y = 0

end_x = 12
end_y = 9

matrix = [
  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
  [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1],
  [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1],
  [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1],
  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
]

# Any value smaller or equal to 0 describes an obstacle
# Any number bigger than 0 describes the weight of a field that can be walked on
# The bigger the number the higher the cost to walk that field

grid = Grid(matrix=matrix)

start = grid.node(12, 1)
end = grid.node(12, 9)
#               x, y

finder = AStarFinder(diagonal_movement=DiagonalMovement.always)
path, runs = finder.find_path(start, end, grid)
# create the finder

print('operations:', runs, 'path length:', len(path))
print(grid.grid_str(path=path, start=start, end=end))


# Funktionen für den Roboter

current_pos = start


def returnNextX():
    global current_pos
    return current_pos.x + 1


def returnNextY():
    global current_pos
    return current_pos.y + 1


def setCurrentPos(x, y):
    global current_pos
    current_pos = grid.node(x, y)


def calculatePath():
    global current_pos, end, grid
    path, runs = finder.find_path(current_pos, end, grid)
    return path


def isObstacle(x, y):
    return matrix[y][x] <= 0

while True:
    # for i in range (0,10):
    #     setCurrentPos(5,5)
    #     calculatePath()
    #     print(returnNextX)
    #     print(" ")
    #     print(returnNextY)
    #     print(grid.grid_str(path=path, start=start, end=end))
    print(current_pos.y)
    time.sleep(1)