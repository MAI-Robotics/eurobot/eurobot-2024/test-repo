#include "Adafruit_VL53L0X.h"
#include "Arduino.h"
#include <Servo.h>
 
Adafruit_VL53L0X lox = Adafruit_VL53L0X();

Servo servoLeft;
Servo servoRight;

int last_range;

void readToF(){
    VL53L0X_RangingMeasurementData_t measure;
    
    Serial.print("Reading a measurement... ");
    lox.rangingTest(&measure, false); // pass in 'true' to get debug data printout!
    
    if (measure.RangeStatus != 4){ // phase failures have incorrect data
        Serial.print("Distance (mm): "); Serial.println(measure.RangeMilliMeter);
        last_range = measure.RangeMilliMeter;
    } else{
        Serial.println("-1");
        last_range = -1;
    }
}

void turn180(){
    servoLeft.write(180);
    servoRight.write(180);
    delay(770);
}

void setup(){
    Serial.begin(115200);

    servoLeft.attach(D3);
    servoRight.attach(D4);

    while (! Serial)
        delay(1);
    
    if (!lox.begin()){
        Serial.println(F("Failed to boot VL53L0X"));
    while(1);
    }

    
    int starting_time = millis();
    
    while(millis() - starting_time < 4000){
      readToF();

      servoLeft.write(180);
      servoRight.write(0);

      while(last_range < 80 && last_range != -1){
        readToF();
        servoLeft.write(90);
        servoRight.write(90);
      }
    }

    servoLeft.write(180);
    servoRight.write(180);
    delay(770);

    starting_time = millis();
    
    while(millis() - starting_time < 4000){
      readToF();

      servoLeft.write(180);
      servoRight.write(0);

      while(last_range < 80 && last_range != -1){
        readToF();
        servoLeft.write(90);
        servoRight.write(90);
      }
    }

    servoLeft.write(90);
    servoRight.write(90);
    
}
 
void loop(){
  readToF();
}