//left encoder pins 
#define LEFT_ENC_A_PHASE 3
#define LEFT_ENC_B_PHASE 2

//right encoder pins 
//#define RIGHT_ENC_A_PHASE 6
//#define RIGHT_ENC_B_PHASE 7                                                                                                                                                                

//left dc motor pins
#define LEFT_DCM_LEFT_PWM 4
#define LEFT_DCM_RIGHT_PWM 5

//right dc motor pins
#define RIGHT_DCM_LEFT_PWM 7
#define RIGHT_DCM_RIGHT_PWM 6