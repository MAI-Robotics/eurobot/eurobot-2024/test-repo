#define ENCODER_OPTIMIZE_INTERRUPTS
#include <Encoder.h>
#include "pins.h"

Encoder encoder(3, 2);

void drive(){
  analogWrite(LEFT_DCM_LEFT_PWM, 180);
  analogWrite(LEFT_DCM_RIGHT_PWM, 0);

  analogWrite(RIGHT_DCM_LEFT_PWM, 0);
  analogWrite(RIGHT_DCM_RIGHT_PWM, 180);
}

void stop(){
  analogWrite(LEFT_DCM_LEFT_PWM, 0);
  analogWrite(LEFT_DCM_RIGHT_PWM, 0);

  analogWrite(RIGHT_DCM_LEFT_PWM, 0);
  analogWrite(RIGHT_DCM_RIGHT_PWM, 0);
}

void setup() {
  Serial.begin(9600);

  pinMode(LEFT_DCM_LEFT_PWM, OUTPUT);
  pinMode(LEFT_DCM_RIGHT_PWM, OUTPUT);

  pinMode(RIGHT_DCM_LEFT_PWM, OUTPUT);
  pinMode(RIGHT_DCM_RIGHT_PWM, OUTPUT);

  delay(1000);
  drive();
  while(encoder.read() < 15287){
    drive();
    Serial.println(encoder.read());
  }
  stop();
  Serial.println(encoder.read());
}

void loop() {

}
